Использование скрипта **Smart Notifications**:

1. Распаковываем все файлы архива в одну папку на сайте

2. Подключаем к сайту jQuery версии 1.7 или новее

3. Подключаем скрипт notifications.js и стиль notifications.css

```
#!html

   <script type="text/javascript" src="notifications_min.js"></script>
   <link rel="stylesheet" type="text/css" href="style/vk/notifications.css">
```


4. Если необходимо, то после подключения скрипта настраиваем его:

```
#!javascript

   <script type="text/javascript">
   notifications.set({
      position: 'bottom-right', // позиция блока с уведомлениями (bottom-right,bottom-left,top-right,top-left ; стандартно bottom-right)
      hide_cross: 1, // скрывать крестик и отображать при наведении на уведомление (0,1 ; стандартно 1)
      del_only_cross: 0, // удалять уведомление только по нажатию на крестик (0,1 ; стандартно 0)
      is_sound: 1, // включить звук нового уведомления (0,1 ; стандартно 1)
      sound_url: 'new_notice.wav',  // адрес звука нового уведомления (0,1 ; стандартно new_notice.wav)
      padding: 10, // отступ блока с уведомлениями от краев страницы (стандартно 10)
      is_autohide: 1, // включить автоскрытие уведомлений (0,1 ; стандартно 1)
      ie_corner: 1, // включить закругления в Internet Explorer (если подключен jquery.corner.js ; стандартно 1)
      time: { // время (1 секунда = 1000)
         showmsg: 500, // время за которое всплывает новое уведомление (стандартно 500)
         hidemsg: 300, // время за которое скрывается уведомление (стандартно 300)
         delmsg: 10000, // время через которое уведомление автоматически скроется (стандартно 10000)
         delsound: 4000 // время через которое удалится звук из DOM страницы (стандартно 4000)
      },
      onshow: function(){
         // функция выполняется при отображении каждого нового уведомления
      },
      onhide: function(){
         // функция выполняется при скрытии каждого уведомления
      }
   });
   </script>
```


5. Если перед скриптом notifications.js подключить скрипт jquery.corner.js, то и в Internet Explorer углы уведомлений будут закругленные

6. Добавление нового уведомления:

```
#!javascript

   notifications.add('Заголовок','Текст уведомления',Действие при нажатии на уведомление)";
```

   Пример:

```
#!javascript

   notifications.add('Новое сообщение','Вам пришло личное сообщение от пользователя ...',function(){alert('onclick');})";
```


7. Функция 
```
#!javascript

notifications.del_all(isanimate)
```
 удаляет все созданные уведомления (если isanimate=1 то уведомления исчезнут анимированно)

8. Функция 
```
#!javascript

notifications.change_position(pos)
```
 изменяет позицию блока с уведомлениями (bottom-right,bottom-left,top-right,top-left)