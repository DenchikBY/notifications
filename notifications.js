/**
 * Smart Notifications
 * Version 1.1 (30.05.2012)
 * Author ����� ����� (Butko Denis) - butko.denis.95@gmail.com
 * Copyright ������ (Author)
 * Requires jQuery 1.7 or later
**/

var notifications = {
   i: 0,
   position: 'bottom-right',
   hide_cross: 1,
   del_only_cross: 0,
   is_sound: 1,
   sound_url: 'new_notice.wav',
   padding: 10,
   is_autohide: 1,
   ie_corner: 1,
   time: {
      showmsg: 500,
      hidemsg: 300,
      delmsg: 10000,
      delsound: 4000
   },
   set_onclick: function(selector,onclick){
      function do_onclick(){
         if(typeof(onclick)==='function'){
            onclick();
         }
      }
      if(notifications.del_only_cross===1){
         jQuery(selector).children(".notice_close").click(function(){
            do_onclick();
            notifications.del(jQuery(this).parent());
         });
      }else{
         jQuery(selector).click(function(){
            do_onclick();
            notifications.del(this);
         }).contextmenu(function(){
            do_onclick();
            notifications.del(this);
            return false;
         });
      }
   },
   sound: function(){
      if(notifications.is_ie){
         sound_obj = jQuery('<embed/>').attr("src", notifications.sound_url).attr("autostart", true).attr("width", "0").attr("height", "0").attr('hidden','true').attr("id", "sound-"+notifications.i);
      }else{
         sound_obj = jQuery('<audio/>').attr("src", notifications.sound_url).attr("autoplay", "autoplay").attr("id", "sound-"+notifications.i);
      }
      jQuery("body").append(sound_obj);
      window.setTimeout('jQuery("#sound-'+notifications.i+'").remove()', notifications.time.delsound);
   },
   add: function(title,text,onclick){
      notifications.i++;
      if(notifications.position===''){
         notifications.position = 'bottom-left';
      }
      pos_arr = notifications.position.split('-');
      if(!document.getElementById('notifications_container')){
         container = jQuery('<div/>').attr('id','notifications_container');
         css_pos = '';
         if(notifications.is_ie){
            css_pos += 'position:absolute;';
         }else{
            css_pos += 'position:fixed;';
         }
         if(pos_arr[0]==='bottom'){
            css_pos += 'bottom:'+notifications.padding+'px;';
         }else if(pos_arr[0]==='top'){
            css_pos += 'top:'+notifications.padding+'px;';
         }
         if(pos_arr[1]==='left'){
            css_pos += 'left:'+notifications.padding+'px;';
         }else if(pos_arr[1]==='right'){
            css_pos += 'right:'+notifications.padding+'px;';
         }
         jQuery(container).attr('style',css_pos);
         jQuery("body").append(container);
      }
      obj = jQuery('<div/>').attr('class','notice').attr('id','notice-'+notifications.i).append('<div class="notice_title">'+title+'</div><div class="notice_close"><span class="cross"></span></div><div class="notice_text">'+text+'</div>');
      if(notifications.hide_cross===1){
         var close_button = jQuery(obj).children(".notice_close");
         close_button.css('display','none');
         jQuery(obj).mouseover(function(){
            close_button.css('display','');
         }).mouseout(function(){
            close_button.css('display','none');
         });
      }
      notifications.set_onclick(obj,onclick);
      if(notifications.is_sound===1){
         notifications.sound(notifications.i);
      }
      if(pos_arr[0]==='bottom'){
         jQuery("#notifications_container").append(obj).css('bottom',-(jQuery(obj).height())+notifications.padding-20).animate({bottom:notifications.padding},notifications.time.showmsg);
      }else if(pos_arr[0]==='top'){
         jQuery("#notifications_container").prepend(obj).css('top',-(jQuery(obj).height())+notifications.padding-20).animate({top:notifications.padding},notifications.time.showmsg);
      }
      if(notifications.is_ie && notifications.ie_corner===1 && typeof(jQuery.fn.corner)==='function'){
         jQuery(obj).corner("round 5px");
      }
      if(typeof(notifications.onshow)==='function'){
         notifications.onshow();
      }
      if(notifications.is_autohide===1){
         window.setTimeout('notifications.del('+notifications.i+')', notifications.time.delmsg);
      }
   },
   del: function(a){
      var type = typeof(a);
      if(type==='object'){
         selector = a;
      }else if(type==='number'){
         selector = "#notice-"+a;
      }
      jQuery(selector).animate({opacity:0,height:'hide'},notifications.time.hidemsg).delay(notifications.time.hidemsg, function(){ jQuery(selector).remove(); });
      if(typeof(notifications.onhide)==='function'){
         notifications.onhide();
      }
   },
   del_all: function(isanimate){
      if(isanimate===1){
         jQuery("#notifications_container").children(".notice").animate({opacity:0,height:'hide'},notifications.time.hidemsg).delay(notifications.time.hidemsg, function(){ jQuery(".notice").remove(); });
      }else{
         jQuery("#notifications_container").children(".notice").remove();
      }
   },
   set: function(settings){
      for (var key in settings){
         var val = settings[key];
         type = typeof(val);
         if(type==='string' || type==='number' || type==='function'){
            notifications[key] = val;
         }else if(type==='object'){
            for (var key1 in settings[key]){
               var val1 = settings[key][key1];
               notifications[key][key1] = val1;
            }
         }
      }
   },
   change_position: function(pos){
      if(pos==='bottom-right' || pos==='bottom-left' || pos==='top-right' || pos==='top-left'){
         cur_pos_arr = notifications.position.split('-');
         pos_arr = pos.split('-');
         css_pos = '';
         if(notifications.is_ie){
            css_pos += 'position:absolute;';
         }else{
            css_pos += 'position:fixed;';
         }
         if(pos_arr[0]==='bottom'){
            css_pos += 'bottom:'+notifications.padding+'px;';
         }else if(pos_arr[0]==='top'){
            css_pos += 'top:'+notifications.padding+'px;';
         }
         if(pos_arr[1]==='left'){
            css_pos += 'left:'+notifications.padding+'px;';
         }else if(pos_arr[1]==='right'){
            css_pos += 'right:'+notifications.padding+'px;';
         }
         notifications.position = pos;
         jQuery("#notifications_container").attr('style',css_pos);
         if(cur_pos_arr[0]!==pos_arr[0]){
            var notices = jQuery("#notifications_container").children(".notice");
            for (var i=1; i<notices.length; i++){
               jQuery("#notifications_container").prepend(notices[i]);
            }
         }
      }
   },
   init: function(){
      notifications.is_ie = '\v'=='v';
      jQuery(window).scroll(function(){
         if(notifications.is_ie && document.getElementById('notifications_container')){
            scrolltop = jQuery(window).scrollTop();
            pos_arr = notifications.position.split('-');
            if(pos_arr[0]==='bottom'){
               jQuery("#notifications_container").css("bottom",-scrolltop);
            }else if(pos_arr[0]==='top'){
               jQuery("#notifications_container").css("top",scrolltop);
            }
         }
      });
      jQuery("#notifications_container").ready(function(){
         if(notifications.is_ie){
            jQuery("#notifications_container").css('position','absolute');
         }else{
            jQuery("#notifications_container").css('position','fixed');
         }
         notice_arr = jQuery("#notifications_container").children(".notice");
         notifications.i = notice_arr.length;
         if(notifications.hide_cross===1){
            var close_button = jQuery(notice_arr).children(".notice_close");
            close_button.css('display','none');
            for (var i=0; i<close_button.length; i++){
               jQuery(close_button[i]).parent().mouseover(function(){
                  jQuery(this).children(".notice_close").css('display','');
               }).mouseout(function(){
                  jQuery(this).children(".notice_close").css('display','none');
               });
            }
         }
         if(notifications.is_autohide===1){
            window.setTimeout('notifications.del(notice_arr)', notifications.time.delmsg);
         }
         notifications.set_onclick(notice_arr);
         if(notifications.is_ie && notifications.ie_corner===1 && typeof(jQuery.fn.corner)==='function'){
            jQuery(notice_arr).corner("round 5px");
         }
      });
   }
};
notifications.init();